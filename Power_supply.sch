EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:microchip1
LIBS:UND2916B
LIBS:LB1836M
LIBS:vrpi
LIBS:sd-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title ""
Date "10 nov 2017"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR053
U 1 1 51DA86E6
P 4350 3600
F 0 "#PWR053" H 4350 3600 30  0001 C CNN
F 1 "GND" H 4350 3530 30  0001 C CNN
F 2 "" H 4350 3600 60  0001 C CNN
F 3 "" H 4350 3600 60  0001 C CNN
	1    4350 3600
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K11
U 1 1 51DA8559
P 4850 2650
F 0 "K11" V 4800 2650 50  0000 C CNN
F 1 "Vout" V 4900 2650 40  0000 C CNN
F 2 "" H 4850 2650 60  0001 C CNN
F 3 "" H 4850 2650 60  0001 C CNN
	1    4850 2650
	0    1    1    0   
$EndComp
$Comp
L CONN_2 P16
U 1 1 51DA84F1
P 3850 3600
F 0 "P16" V 3800 3600 40  0000 C CNN
F 1 "Vin" V 3900 3600 40  0000 C CNN
F 2 "" H 3850 3600 60  0001 C CNN
F 3 "" H 3850 3600 60  0001 C CNN
	1    3850 3600
	-1   0    0    1   
$EndComp
$Comp
L GND3V3 #PWR054
U 1 1 51D57B86
P 5400 5900
F 0 "#PWR054" H 5400 5900 40  0001 C CNN
F 1 "GND3V3" H 5400 5830 50  0000 C CNN
F 2 "" H 5400 5900 60  0001 C CNN
F 3 "" H 5400 5900 60  0001 C CNN
	1    5400 5900
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR055
U 1 1 51D57B81
P 4350 5750
F 0 "#PWR055" H 4350 5710 30  0001 C CNN
F 1 "+3.3V" H 4350 5860 30  0000 C CNN
F 2 "" H 4350 5750 60  0001 C CNN
F 3 "" H 4350 5750 60  0001 C CNN
	1    4350 5750
	1    0    0    -1  
$EndComp
$Comp
L R R19
U 1 1 51D53D89
P 4600 5800
F 0 "R19" V 4680 5800 50  0000 C CNN
F 1 "460" V 4600 5800 50  0000 C CNN
F 2 "" H 4600 5800 60  0001 C CNN
F 3 "" H 4600 5800 60  0001 C CNN
	1    4600 5800
	0    -1   -1   0   
$EndComp
$Comp
L LED D12
U 1 1 51D53D83
P 5100 5800
F 0 "D12" H 5100 5900 50  0000 C CNN
F 1 "LED" H 5100 5700 50  0000 C CNN
F 2 "" H 5100 5800 60  0001 C CNN
F 3 "" H 5100 5800 60  0001 C CNN
	1    5100 5800
	1    0    0    -1  
$EndComp
Text Notes 7250 1350 0    60   ~ 0
LD1117A
$Comp
L GND3V3 #PWR056
U 1 1 518B8935
P 7350 1900
F 0 "#PWR056" H 7350 1900 40  0001 C CNN
F 1 "GND3V3" H 7350 1830 50  0000 C CNN
F 2 "" H 7350 1900 60  0001 C CNN
F 3 "" H 7350 1900 60  0001 C CNN
	1    7350 1900
	1    0    0    -1  
$EndComp
$Comp
L GND3V3 #PWR057
U 1 1 518B8927
P 7550 2550
F 0 "#PWR057" H 7550 2550 40  0001 C CNN
F 1 "GND3V3" H 7550 2480 50  0000 C CNN
F 2 "" H 7550 2550 60  0001 C CNN
F 3 "" H 7550 2550 60  0001 C CNN
	1    7550 2550
	1    0    0    -1  
$EndComp
Text GLabel 4050 2050 2    60   Input ~ 0
Vservo
$Comp
L +5V #PWR058
U 1 1 518B7C3B
P 4950 2200
F 0 "#PWR058" H 4950 2290 20  0001 C CNN
F 1 "+5V" H 4950 2290 30  0000 C CNN
F 2 "" H 4950 2200 60  0001 C CNN
F 3 "" H 4950 2200 60  0001 C CNN
	1    4950 2200
	1    0    0    -1  
$EndComp
$Comp
L GNDSERVO #PWR059
U 1 1 518B7578
P 3400 2550
F 0 "#PWR059" H 3400 2550 40  0001 C CNN
F 1 "GNDSERVO" H 3400 2480 50  0000 C CNN
F 2 "" H 3400 2550 60  0001 C CNN
F 3 "" H 3400 2550 60  0001 C CNN
	1    3400 2550
	1    0    0    -1  
$EndComp
Text GLabel 5650 2050 2    60   Input ~ 0
Vout
$Comp
L DIODE D13
U 1 1 518B7519
P 3400 2300
F 0 "D13" H 3400 2400 40  0000 C CNN
F 1 "DIODE" H 3400 2200 40  0000 C CNN
F 2 "" H 3400 2300 60  0001 C CNN
F 3 "" H 3400 2300 60  0001 C CNN
	1    3400 2300
	0    -1   -1   0   
$EndComp
$Comp
L R R22
U 1 1 518B7514
P 3050 2050
F 0 "R22" V 3130 2050 50  0000 C CNN
F 1 "0.25" V 3050 2050 50  0000 C CNN
F 2 "" H 3050 2050 60  0001 C CNN
F 3 "" H 3050 2050 60  0001 C CNN
	1    3050 2050
	0    -1   1    0   
$EndComp
$Comp
L DIODE D14
U 1 1 518B7510
P 3700 2050
F 0 "D14" H 3700 2150 40  0000 C CNN
F 1 "DIODE" H 3700 1950 40  0000 C CNN
F 2 "" H 3700 2050 60  0001 C CNN
F 3 "" H 3700 2050 60  0001 C CNN
	1    3700 2050
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR060
U 1 1 518B7501
P 2700 1850
F 0 "#PWR060" H 2700 1940 20  0001 C CNN
F 1 "+5V" H 2700 1940 30  0000 C CNN
F 2 "" H 2700 1850 60  0001 C CNN
F 3 "" H 2700 1850 60  0001 C CNN
	1    2700 1850
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR061
U 1 1 518B7144
P 7200 1800
F 0 "#PWR061" H 7200 1890 20  0001 C CNN
F 1 "+5V" H 7200 1890 30  0000 C CNN
F 2 "" H 7200 1800 60  0001 C CNN
F 3 "" H 7200 1800 60  0001 C CNN
	1    7200 1800
	1    0    0    -1  
$EndComp
$Comp
L CONN_5 P17
U 1 1 518B706D
P 3850 4100
F 0 "P17" V 3800 4100 50  0000 C CNN
F 1 "USB" V 3900 4100 50  0000 C CNN
F 2 "" H 3850 4100 60  0001 C CNN
F 3 "" H 3850 4100 60  0001 C CNN
	1    3850 4100
	-1   0    0    1   
$EndComp
NoConn ~ 4250 4000
$Comp
L GND #PWR062
U 1 1 518B706C
P 4450 3950
F 0 "#PWR062" H 4450 3950 30  0001 C CNN
F 1 "GND" H 4450 3880 30  0001 C CNN
F 2 "" H 4450 3950 60  0001 C CNN
F 3 "" H 4450 3950 60  0001 C CNN
	1    4450 3950
	1    0    0    -1  
$EndComp
Text GLabel 4400 4100 2    60   Input ~ 0
D+
Text GLabel 4400 4200 2    60   Input ~ 0
D-
Entry Wire Line
	19000 100  19100 200 
$Comp
L GNDRP #PWR063
U 1 1 50B44EBE
P 10500 2200
F 0 "#PWR063" H 10500 2200 40  0001 C CNN
F 1 "GNDRP" H 10500 2130 50  0000 C CNN
F 2 "" H 10500 2200 60  0001 C CNN
F 3 "" H 10500 2200 60  0001 C CNN
	1    10500 2200
	1    0    0    -1  
$EndComp
$Comp
L GND3V3 #PWR064
U 1 1 50B44EB6
P 10150 2200
F 0 "#PWR064" H 10150 2200 40  0001 C CNN
F 1 "GND3V3" H 10150 2130 50  0000 C CNN
F 2 "" H 10150 2200 60  0001 C CNN
F 3 "" H 10150 2200 60  0001 C CNN
	1    10150 2200
	1    0    0    -1  
$EndComp
$Comp
L GNDSERVO #PWR065
U 1 1 50B44EB0
P 9800 2200
F 0 "#PWR065" H 9800 2200 40  0001 C CNN
F 1 "GNDSERVO" H 9800 2130 50  0000 C CNN
F 2 "" H 9800 2200 60  0001 C CNN
F 3 "" H 9800 2200 60  0001 C CNN
	1    9800 2200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR066
U 1 1 50B44E9E
P 9450 2350
F 0 "#PWR066" H 9450 2350 30  0001 C CNN
F 1 "GND" H 9450 2280 30  0001 C CNN
F 2 "" H 9450 2350 60  0001 C CNN
F 3 "" H 9450 2350 60  0001 C CNN
	1    9450 2350
	1    0    0    -1  
$EndComp
$Comp
L C C11
U 1 1 50AF36D0
P 7550 2300
F 0 "C11" H 7600 2400 50  0000 L CNN
F 1 "0.1uF" H 7600 2200 50  0000 L CNN
F 2 "" H 7550 2300 60  0001 C CNN
F 3 "" H 7550 2300 60  0001 C CNN
	1    7550 2300
	1    0    0    1   
$EndComp
$Comp
L GND3V3 #PWR067
U 1 1 50AF36C4
P 8050 2550
F 0 "#PWR067" H 8050 2550 40  0001 C CNN
F 1 "GND3V3" H 8050 2480 50  0000 C CNN
F 2 "" H 8050 2550 60  0001 C CNN
F 3 "" H 8050 2550 60  0001 C CNN
	1    8050 2550
	1    0    0    -1  
$EndComp
$Comp
L C C12
U 1 1 4FB5EB6D
P 8050 2300
F 0 "C12" H 8100 2400 50  0000 L CNN
F 1 "10uF" H 8100 2200 50  0000 L CNN
F 2 "" H 8050 2300 60  0001 C CNN
F 3 "" H 8050 2300 60  0001 C CNN
	1    8050 2300
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR068
U 1 1 4FB5EB6C
P 8050 1850
F 0 "#PWR068" H 8050 1810 30  0001 C CNN
F 1 "+3.3V" H 8050 1960 30  0000 C CNN
F 2 "" H 8050 1850 60  0001 C CNN
F 3 "" H 8050 1850 60  0001 C CNN
	1    8050 1850
	1    0    0    -1  
$EndComp
$Comp
L SWITCH_INV2 SW3
U 1 1 4FB5EB6B
P 5850 4050
F 0 "SW3" H 5650 4300 50  0000 C CNN
F 1 "SWITCH_INV2" H 5700 4000 50  0000 C CNN
F 2 "" H 5850 4050 60  0001 C CNN
F 3 "" H 5850 4050 60  0001 C CNN
	1    5850 4050
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K10
U 1 1 4FB5EB62
P 7450 1500
F 0 "K10" V 7400 1500 50  0000 C CNN
F 1 "Reg3.3V" V 7500 1500 40  0000 C CNN
F 2 "" H 7450 1500 60  0001 C CNN
F 3 "" H 7450 1500 60  0001 C CNN
	1    7450 1500
	0    -1   -1   0   
$EndComp
$Comp
L MOSFET_P Q3
U 1 1 540846DF
P 7850 4500
F 0 "Q3" H 7850 4690 60  0000 R CNN
F 1 "MOSFET_P" V 8050 4700 60  0000 R CNN
F 2 "~" H 7850 4500 60  0000 C CNN
F 3 "~" H 7850 4500 60  0000 C CNN
	1    7850 4500
	0    -1   -1   0   
$EndComp
$Comp
L DMMT5401 U5
U 1 1 5408583D
P 7850 5500
F 0 "U5" H 7850 5650 60  0000 R CNN
F 1 "DMMT5401" H 8100 5750 60  0000 R CNN
F 2 "~" H 7600 5500 60  0000 C CNN
F 3 "~" H 7600 5500 60  0000 C CNN
	1    7850 5500
	1    0    0    -1  
$EndComp
$Comp
L R R20
U 1 1 540858EF
P 7500 6250
F 0 "R20" V 7580 6250 40  0000 C CNN
F 1 "10k" V 7507 6251 40  0000 C CNN
F 2 "~" V 7430 6250 30  0000 C CNN
F 3 "~" H 7500 6250 30  0000 C CNN
	1    7500 6250
	1    0    0    -1  
$EndComp
$Comp
L R R21
U 1 1 54085908
P 8200 6250
F 0 "R21" V 8280 6250 40  0000 C CNN
F 1 "47k" V 8207 6251 40  0000 C CNN
F 2 "~" V 8130 6250 30  0000 C CNN
F 3 "~" H 8200 6250 30  0000 C CNN
	1    8200 6250
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR069
U 1 1 54085C5A
P 7500 6550
F 0 "#PWR069" H 7500 6550 30  0001 C CNN
F 1 "GND" H 7500 6480 30  0001 C CNN
F 2 "" H 7500 6550 60  0000 C CNN
F 3 "" H 7500 6550 60  0000 C CNN
	1    7500 6550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR070
U 1 1 54085CB6
P 8200 6550
F 0 "#PWR070" H 8200 6550 30  0001 C CNN
F 1 "GND" H 8200 6480 30  0001 C CNN
F 2 "" H 8200 6550 60  0000 C CNN
F 3 "" H 8200 6550 60  0000 C CNN
	1    8200 6550
	1    0    0    -1  
$EndComp
Text Notes 7650 4200 0    60   ~ 0
Ideal diode
Text Notes 4750 1800 0    60   ~ 0
DC Motor Current Limit
Text Notes 3000 1800 0    60   ~ 0
Servo Motor Current Limit
Text Notes 7550 1100 0    60   ~ 0
Reg3V3
Text Notes 4650 5550 0    60   ~ 0
PWR LED
$Comp
L +5V #PWR071
U 1 1 5408CC7A
P 8500 4250
F 0 "#PWR071" H 8500 4340 20  0001 C CNN
F 1 "+5V" H 8500 4340 30  0000 C CNN
F 2 "" H 8500 4250 60  0000 C CNN
F 3 "" H 8500 4250 60  0000 C CNN
	1    8500 4250
	1    0    0    -1  
$EndComp
$Comp
L VRPI #PWR072
U 1 1 5408CF7D
P 8600 4250
F 0 "#PWR072" H 8600 4350 30  0001 C CNN
F 1 "VRPI" H 8600 4350 30  0000 C CNN
F 2 "~" H 8600 4250 60  0000 C CNN
F 3 "~" H 8600 4250 60  0000 C CNN
	1    8600 4250
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR073
U 1 1 5408E57C
P 4750 2200
F 0 "#PWR073" H 4750 2300 30  0001 C CNN
F 1 "VCC" H 4750 2300 30  0000 C CNN
F 2 "" H 4750 2200 60  0000 C CNN
F 3 "" H 4750 2200 60  0000 C CNN
	1    4750 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 4400 7650 4400
Wire Wire Line
	8200 6500 8200 6550
Wire Wire Line
	7500 6500 7500 6550
Connection ~ 8200 5950
Wire Wire Line
	7850 4700 7850 5950
Wire Wire Line
	7850 5950 8200 5950
Connection ~ 7500 5850
Wire Wire Line
	8200 5700 8200 6000
Connection ~ 7700 5850
Wire Wire Line
	7700 5700 7700 5850
Wire Wire Line
	8000 5850 8000 5700
Wire Wire Line
	7500 5850 8000 5850
Wire Wire Line
	7500 5700 7500 6000
Wire Wire Line
	8200 4400 8200 5300
Wire Wire Line
	5350 4300 5350 4050
Connection ~ 5350 4300
Wire Wire Line
	4850 2050 4850 2300
Wire Wire Line
	4350 5750 4350 5800
Wire Wire Line
	7450 1950 8050 1950
Wire Wire Line
	4850 2050 5650 2050
Wire Wire Line
	3400 2500 3400 2550
Connection ~ 3400 2050
Wire Wire Line
	3400 2100 3400 2050
Wire Wire Line
	4050 2050 3900 2050
Wire Wire Line
	4250 4300 5350 4300
Connection ~ 9800 2100
Wire Wire Line
	10150 2100 10150 2200
Wire Wire Line
	7550 2500 7550 2550
Wire Wire Line
	7200 2050 7550 2050
Wire Wire Line
	8050 2500 8050 2550
Connection ~ 8050 1950
Wire Wire Line
	8050 1850 8050 2100
Wire Wire Line
	7450 1850 7450 1950
Wire Wire Line
	5350 4050 6350 4050
Wire Wire Line
	6350 4050 6350 4200
Wire Wire Line
	7350 1850 7350 1900
Wire Wire Line
	7550 1850 7550 2100
Connection ~ 7550 2050
Wire Wire Line
	9450 2100 9450 2350
Wire Wire Line
	9800 2200 9800 2100
Wire Wire Line
	10500 2100 9450 2100
Wire Wire Line
	10500 2100 10500 2200
Connection ~ 10150 2100
Wire Wire Line
	4250 4200 4400 4200
Wire Wire Line
	4250 4100 4400 4100
Wire Wire Line
	4450 3900 4250 3900
Wire Wire Line
	4450 3950 4450 3900
Wire Wire Line
	7200 1800 7200 2100
Wire Wire Line
	3300 2050 3500 2050
Wire Wire Line
	2700 2050 2800 2050
Wire Wire Line
	2700 1850 2700 2050
Wire Wire Line
	5400 5800 5300 5800
Wire Wire Line
	5400 5900 5400 5800
Wire Wire Line
	4850 5800 4900 5800
Wire Wire Line
	4950 2200 4950 2300
Wire Wire Line
	4750 2200 4750 2300
Wire Wire Line
	8050 4400 8500 4400
Wire Wire Line
	6350 3750 6350 3550
Wire Wire Line
	6350 3550 5350 3550
Wire Wire Line
	5350 3550 5350 3850
Connection ~ 7500 4400
Wire Wire Line
	8500 4250 8500 4450
Connection ~ 8200 4400
Connection ~ 8500 4400
Wire Wire Line
	6700 3950 6350 3950
Wire Wire Line
	4200 3500 4350 3500
Wire Wire Line
	4350 3500 4350 3600
Wire Wire Line
	7500 4400 7500 5300
$Comp
L DIODE D11
U 1 1 54092BB3
P 5000 3850
F 0 "D11" H 5000 3950 40  0000 C CNN
F 1 "DIODE" H 5000 3750 40  0000 C CNN
F 2 "~" H 5000 3850 60  0000 C CNN
F 3 "~" H 5000 3850 60  0000 C CNN
	1    5000 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3850 5200 3850
Wire Wire Line
	4800 3850 4600 3850
Wire Wire Line
	4600 3850 4600 3700
Wire Wire Line
	4600 3700 4200 3700
$Comp
L VCC #PWR074
U 1 1 540EF5AC
P 6700 3450
F 0 "#PWR074" H 6700 3550 30  0001 C CNN
F 1 "VCC" H 6700 3550 30  0000 C CNN
F 2 "" H 6700 3450 60  0000 C CNN
F 3 "" H 6700 3450 60  0000 C CNN
	1    6700 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 3450 6700 3950
$Comp
L C C13
U 1 1 540F293C
P 7200 2300
F 0 "C13" H 7200 2400 40  0000 L CNN
F 1 "100uF" H 7206 2215 40  0000 L CNN
F 2 "~" H 7238 2150 30  0000 C CNN
F 3 "~" H 7200 2300 60  0000 C CNN
	1    7200 2300
	1    0    0    -1  
$EndComp
Connection ~ 7200 2050
$Comp
L GND3V3 #PWR075
U 1 1 540F29A2
P 7200 2550
F 0 "#PWR075" H 7200 2550 40  0001 C CNN
F 1 "GND3V3" H 7200 2480 50  0000 C CNN
F 2 "~" H 7200 2550 60  0000 C CNN
F 3 "~" H 7200 2550 60  0000 C CNN
	1    7200 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 2500 7200 2550
$Comp
L CONN_3 P18
U 1 1 540F43A4
P 8600 4800
F 0 "P18" V 8550 4800 50  0000 C CNN
F 1 "Power RPi" V 8650 4800 40  0000 C CNN
F 2 "" H 8600 4800 60  0000 C CNN
F 3 "" H 8600 4800 60  0000 C CNN
	1    8600 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	8600 4250 8600 4450
NoConn ~ 8700 4450
$Comp
L CONN_2 P19
U 1 1 5480263F
P 3850 4600
F 0 "P19" V 3800 4600 40  0000 C CNN
F 1 "5V" V 3900 4600 40  0000 C CNN
F 2 "" H 3850 4600 60  0000 C CNN
F 3 "" H 3850 4600 60  0000 C CNN
	1    3850 4600
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR076
U 1 1 54802658
P 4350 4800
F 0 "#PWR076" H 4350 4800 30  0001 C CNN
F 1 "GND" H 4350 4730 30  0001 C CNN
F 2 "" H 4350 4800 60  0000 C CNN
F 3 "" H 4350 4800 60  0000 C CNN
	1    4350 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 4700 4350 4700
Wire Wire Line
	4350 4700 4350 4800
Wire Wire Line
	4200 4500 4950 4500
Wire Wire Line
	4950 4500 4950 4300
Connection ~ 4950 4300
$EndSCHEMATC
